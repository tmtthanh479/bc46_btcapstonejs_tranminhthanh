var filteredProducts = []; // Lưu trữ danh sách sản phẩm đã lọc
function renderProductList(productArr) {
  var contentHTML = "";
  productArr.forEach(function (item) {
    //  forEach(function (item) là đi duyệt hàm
    // item đại diện cho từng phần tử trong productArr
    var content = `
    <div class="max-w-xs w-full bg-white shadow-lg rounded-lg overflow-hidden m-4 hover:scale-[1.05] transition-transform duration-300 ease-in-out">
    <img src="${item.img}" alt="${item.name}" class="cart-item-image">
    <div class="p-4">
      <h2 id="${item.name}" class="font-bold text-xl mb-2">${item.name}</h2>
      <div id ="${item.price}" class=" price-price text-gray-900 font-semibold">Price: <span class="text-gray-600">${item.price}</span></div>
      <div id="${item.screen}" class=" screen-screen text-gray-900 font-semibold">Screen: <span class="text-gray-600">${item.screen}</span></div>
      <div class="text-gray-900 font-semibold">Back Camera: <span class="text-gray-600">${item.backCamera}</span></div>
      <div   class="text-gray-900 font-semibold">Front Camera: <span class="text-gray-600">${item.frontCamera}</span></div>
      <div class="text-gray-900 font-semibold">Description: <span class="text-gray-600">${item.desc}</span></div>
      <div  class="text-gray-900 font-semibold">Type: <span class="text-gray-600">${item.type}</span></div>
      <div  class="text-gray-900 font-semibold">Type: <span class="text-gray-600">${item.id}</span></div>

      <button id= "button-cart"  class="bg-green-500 hover:bg-green-600 text-white font-bold py-2 px-4 rounded mt-1">
      thêm vào giỏ hàng</button>
    </div>
  </div>
  
  

    `;
    contentHTML += content;

  });

  document.getElementById("tblDanhSachSP").innerHTML = contentHTML;
  // show thông tin lên cart
  var addToCartButtons = document.querySelectorAll("#button-cart"); // button "thêm vào giỏ hàng"
  addToCartButtons.forEach(function (button) {
    button.addEventListener("click", function () {
      // lấy thông tinh
      var productElement = this.parentNode;
    
      var ten = productElement.querySelector("h2");
      var gia = productElement.querySelector(".price-price");
      var manHinh = productElement.querySelector(".screen-screen");
      //  tạo object để add tất cả item( thuộc tính của phone) vào giỏ hàng
      var item = {
        name: ten.innerText,
        price: gia.innerText,
        screen: manHinh.innerText,
     
      };

      addToCart(item);
    });
  });
}
// funtion tăng giảm số lượng
function showCart() {
  var cartItemsHTML = [];
  var totalPrice = 0; // Biến lưu trữ tổng giá tiền
  cart.forEach(function (item) {
    var cartItemHTML = `
      <div class="cart-item">
        <span> Tên sản phẩm: ${item.name}</span><hr />
        <span> Giá: ${item.price}</span> <hr />
        <span>  ${item.screen}</span> <hr />
        <div class="quantity-buttons">
          <button class="quantity-button-plus">+</button>
          <span class="quantity">${item.quantity}</span>
          <button class="quantity-button-minus">-</button> <hr />
          </div>
          <button data-index="${item.name}"  class="button-remove">remove</button>
      </div>
    `;

    cartItemsHTML.push(cartItemHTML);
    totalPrice += parseInt(item.price.replace(/[^0-9]/g, "")) * item.quantity; // Tính tổng giá tiền của từng sản phẩm
  });

  document.getElementById("total").innerHTML = "Total: " + totalPrice; // Hiển thị tổng giá tiền
  document.getElementById("product-cart-content").innerHTML = cartItemsHTML;
  document.getElementById("product-cart-content").innerHTML = cartItemsHTML.join(""); // xóa dấu ","" khi render 2 sản phẩm trở lên



  // theo dõi user khi user click vào nút "+" và "-"
  var plusButtons = document.querySelectorAll(".quantity-button-plus");
  var minusButtons = document.querySelectorAll(".quantity-button-minus");

//  thì sau đó chạy function
  plusButtons.forEach(function (button) {
    button.addEventListener("click", function () {
      // cần biết plusButtons thành array dùng Array.from(plusButtons)
      var itemIndex = Array.from(plusButtons).indexOf(button); // vì indexOf chỉ dùng được với array
      increaseQuantity(itemIndex);
    });
  });

  minusButtons.forEach(function (button) {
    button.addEventListener("click", function () {
      var itemIndex = Array.from(minusButtons).indexOf(button);
      decreaseQuantity(itemIndex);
    });
  });

 // Lắng nghe sự kiện click vào nút "remove"
 var removeButtons = document.querySelectorAll(".button-remove");
 removeButtons.forEach(function (button) {
  button.addEventListener("click", function () {
    var itemName = button.getAttribute("data-index");
    deleteProduct(itemName); // gọi đến function deleteProduct(index)  bên index.js
  });
});

  console.log(cart); // Hiển thị thông tin giỏ hàng trong console

}
