var cart = [];

if (dataJson != null) {
  var dataJson = localStorage.getItem("cart");
cart = JSON.parse(dataJson)
showCart()
}


// I. Lấy api
axios({
  url: "https://63e370ad65ae4931770ef4f2.mockapi.io/product",
  method: "GET",
})
  // object có thể nhiều nhưng chỉ lấy data
  .then(function (res) {
    // render giao diện
    originalProductArr = res.data; // nếu user chọn tất cả thì render hết dòng 11
    filteredProducts = originalProductArr; // cho 2 cái bằng nhau để lọc || bên controller.js
    renderProductList(res.data); // Hiển thị danh sách sản phẩm ban đầu
  })
  .catch(function (err) {});

// II. Chức Năng search(filter iphone/samsung)
function search() {
  var selectElement = document.getElementById("product-filter");

  //filteredProducts là đã filter
  // originalProductArr chứa tất cả sản phẩm
  // => cho 2 cái = nhau rồi dùng hàm .filter để lọc
  var selectedType = selectElement.value; // đổi html thành value để lọc

  if (selectedType === "samsung") {
    filteredProducts = originalProductArr.filter(function (item) {
      // hàm .filter để lọc là true hay false
      return item.type === "Samsung";
    });
  } else if (selectedType === "iphone") {
    filteredProducts = originalProductArr.filter(function (item) {
      return item.type.toLowerCase() === "iphone";
    });
  } else {
    filteredProducts = originalProductArr; // originalProductArr đang chứa tất cả sản phẩm|| đây là trường hợp không là iphone hay samsung thì nó in ra hết
  }

  renderProductList(filteredProducts);
}

// III. Chức năng bỏ vào cart
var cart = [];
function addToCart(item) {
  // Kiểm tra xem sản phẩm đã tồn tại trong giỏ hàng chưa
  var existingItem = cart.find(function (cartItem) {     //Nếu tìm thấy phần tử trong giỏ hàng có cùng name với item, thì existingItem sẽ trở thành phần tử đó.
    return cartItem.name === item.name; // dùng name vì name đang là duy nhất || là để xác định coi nó là sản phầm nào. kiểu id
  });

  if (existingItem) {
    // Nếu sản phẩm đã tồn tại trong giỏ hàng, tăng số lượng lên 1
    existingItem.quantity++;
  } else {
    // Nếu sản phẩm chưa tồn tại trong giỏ hàng, tạo đối tượng mới và thêm vào mảng cart
    var cartItem = {
      img: item.img,
      name: item.name,
      price: item.price,
      screen: item.screen,

      quantity: 1,
    };
    cart.push(cartItem);
  }

  // Hiển thị(render) thông tin giỏ hàng
  showCart();

// Lưu  Local Storage
  var cartData = JSON.stringify(cart);
  localStorage.setItem("cart", cartData);
// xóa giỏ hàng
  document.getElementById("XoaGioHang").addEventListener("click", function() { // tạo dữ kiện khi user click vào button XoaGioHang
    cart = []; // đưa tất cả phần tử cart thành một mảng rỗng
    showCart(); // render lại giao diện
  });
  
}
// tăng số lượng
function increaseQuantity(index) {
//  index được xác định qua  plusButtons ở controller.js
  cart[index].quantity++;
  showCart();
}
// giảm số lượng
function decreaseQuantity(index) {
  //  index được xác định qua  plusButtons ở controller.js
  if (cart[index].quantity > 1) {
    cart[index].quantity--;
  } else {
    cart.splice(index, 1); // Xóa sản phẩm khỏi giỏ hàng nếu số lượng là 1
  }
  showCart();
}

// IV. xóa cart
/*function xoaSinhVien(id) {
  console.log(id);
  // splice findIndex
  var index = dssv.findIndex(function (item) {
    console.log(" item", item);
    return item.ma == id;
  });
  dssv.splice(index, 1);
  renderDSSV(dssv);
}*/ 
// function deleteProduct(index) {

    
//     cart.splice(index, 1);
//     showCart();
 
//   }

